<?php

define('INPUT_ERROR_REQUIRED', '<em>%s</em> is required'); 
define('INPUT_ERROR_UNDEFINED', '<em>%s</em> does not exist');
define('INPUT_ERROR_NAN','<em>%s</em> <strong>%s</strong> should be a number');
define('INPUT_ERROR_NONALPHANUMERIC',
'%s can only contain letters, numbers, slashes, ampersands (&), hyphens (-) and underscores (_)');
define('INPUT_ERROR_MARKUP','%s is not allowed to contain HTML tags');
define('INPUT_ERROR_LENGTH','%s is %d characters, but the maximum allowed is %d');
define('INPUT_ERROR_DUPLICATE_CATEGORY',
'There is already a category called <em>"%s"</em> under <em>"%s"</em>. Either choose a different parent category or call it something else.');
define('INPUT_ERROR_DATE_FLEXIBLE','The <em>%s</em> <strong>\'%s\'</strong> does not look like a date. Dates should be in the format day/month/year (you can leave out the year if the date is this year), or you can enter a description, for example "30 days", "2 weeks ago" or "next Tuesday"');

define('INPUT_ERROR_EMAIL','<em>email</em> <strong>%s</strong> does not look like a valid email address');

?>