# WebApp

This is a very simple web framework in PHP.

**WARNING: While this code is occasionally maintained, it is no longer being actively developed. Use at your own risk!**

## Example

The basic idea is to pass an array of regular expressions to match URL paths to controller functions:

        require('path/to/webapp.php');

        // Instantiate WebApp
        $app_name= 'Test';
        $base_url = '';
        $tmpl_dir = 'views/';
        $controller_dir = 'controllers/';
        $myapp = new WebApp($app_name,$base_url,$tmpl_dir,$controller_dir);

        // Make an array of URLs to controller function mappings
        $urls = array(array('/^\/$/','test.controller.php'));

        // Run the URLs through WebApp
        $myapp->run($urls);

It will look for functions named after a combination of the controller name and HTTP method. For example `controllers/test.controller.php` could contain:

        function test_GET(){

          // Get access to $myapp
          global $myapp;

          // Assign a template to be rendered
          $myapp->template_src = 'test.php';

          // Add variables that will be passed to the template
          $myapp->template->message = 'hello world!';
        }

## Database connections

This library also contains an ezSQL based database abstraction layer called `SuperDB`. To connect to it and have the conection available to the app you need to give it a path to a file containg a `dbconnect` function. Fo example, in your index file you might have:

        $App->dbconnect('path/to/dbconnect.php');

Then `dbconnect.php` would contain a function like:

        function dbconnect(){
          $db = new SuperDB(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
          return $db;
        }

The `DB_*` constants would need to be defined before this function is invoked.

`SuperDB` contains a few methods to make generating complex queries a bit easier, see the source for more information.
