<?php


function send_email($to,$from,$subject,$content,$data=false,$html=false){
    
    # Check if a template
    if(preg_match('/\.php$/',$content) && is_file($content)){

        # If there's data, copy it to the current scope
        if($data) foreach($data as $k=>$v) $$k = $v;

        # Include the template, buffering the output and capturing it
        ob_start();
        include($content);
        $content = ob_get_contents();
        ob_end_clean();
    } 
    
    # General headers
    $headers = '';
    $headers .= "Return-Path: $from\n";
    $headers .= "Sender: $from\n";
    $headers .= "X-Sender: $from\n";
    $headers .= "From: $from\n";
    $headers .= "Reply-To: $from\n";
    
    # HTML or plain text?
    if($html){
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    
    } else {
        $headers .= "Content-type: text/plain; charset=iso-8859-1\n";
    }

    # Send the message
    if(FAKE_EMAIL_MODE){
        if(is_writable(TMP_DIR)){
            $str = "Subject:$subject\n$headers\n\n".ltrim($content);
            $fname = date('Y-m-d_H-i-s').'_'.str_sanitize($subject);
            $fp = fopen(TMP_DIR."$fname.eml",'wb+');
            fwrite($fp,$str);
            fclose($fp);
        } else {
            trigger_error('Test email cannot be written to '.TMP_DIR,E_USER_ERROR);
        }
    } else {
        mail($to,$subject,$content,$headers,'-f '.$from);
    }

    return true;
}
?>