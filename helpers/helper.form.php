<?php

/**
 * Convert a filed name into a human friednly value e.g. start_date -> Start date
 *
 * @param string $field
 * @return string
 */
function humanize_field($field){
    // Remove trailing id
    $field = trim(preg_replace('/_id$/', '', $field));
    return ucfirst(str_replace('_', ' ', $field));

}
/**
 * Validate values in array are not empty strings
 * Adds errors to `App->post_errors`
 *
 * @param array $input The values to check
 * @param array $required_fields The required key names
 * @param string $context The type of object for generating error messages
 * @return bool
 */
function validate_required($input, $required_fields, $context=false)
{
    global $App;

    $ok = true;
    foreach ($required_fields as $r) {
        if (!isset($input[$r]) || trim($input[$r]) == '') {
            $App->add_post_error(
                $context ? $context . '_' . $r : $r,
                sprintf(INPUT_ERROR_REQUIRED, humanize_field($context . ' ' . $r))
            );
            $ok = false;
        }
    }
    return $ok;
}
/**
 * Validate lengths of values
 * Adds errors to `App->post_errors`
 *
 * @param array $input The values to check
 * @param array $lengths The maximum lengths where $key => $length
 * @param string $context The type of object for generating error messages
 * @return bool
 */
function validate_lengths($input, $lengths, $context='') {

    global $App;
    $ok = true;
    foreach ($lengths as $k=>$v) {
        $len = strlen($input[$k]);
        if ($input[$k] && $len > $v) {
            $App->add_post_error(
                $context ? $context . '_' . $k : $k,
                sprintf(INPUT_ERROR_LENGTH, humanize_field($context . ' ' . $k), $len, $v)
            );
            $ok =false;
        }
    }

    return $ok;
}
/**
 * Validate values are numeric and within constraints
 * Adds errors to `App->post_errors`
 *
 * @param array $input The values to check
 * @param array $constraints The constraints where $key => array($min, $max)
 * @param string $context The type of object for generating error messages
 * @return bool
 */
function validate_numerics($input, $constraints, $context='') {

    global $App;
    $ok = true;
    foreach ($constraints as $k => $v) {
        if (!is_numeric($input[$k])) {
            $App->add_post_error(
                $context ? $context . '_' . $k : $k,
                sprintf(INPUT_ERROR_NUMERIC, humanize_field($context . ' ' . $k), $input[$k])
            );
            $ok = false;
            continue;
        }
        if (is_array($v) && count($v) == 2) {
            list($min, $max) = $v;
            if ($input[$k] < $min) {
                $App->add_post_error(
                    $context ? $context . '_' . $k : $k,
                    sprintf(INPUT_ERROR_NUMERIC_MIN, humanize_field($context . ' ' . $k), $min, $v)
                );
                $ok = false;
            }
            elseif ($input[$k] > $max) {
                $App->add_post_error(
                    $context ? $context . '_' . $k : $k,
                    sprintf(INPUT_ERROR_NUMERIC_MAX, humanize_field($context . ' ' . $k), $max, $v)
                );
                $ok = false;
            }
        }
    }

    return $ok;
}

/**
 * Basic date validation
 */
function validate_date($str) {

    $ts = strtotime_uk($str);
    if ($ts === false || $ts === -1) return false;
    return true;
}

/**
 * Basic email validation
 */
function valid_email($email)
{
    $regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
    if (!preg_match($regexp, $email)) return false;
    return true;
}

/**
 * Return selected attribute if 2 values equal
 */
function is_selected_option($a, $b) {
    return $a == $b ? ' selected="selected"' : '';
}

/**
 * Return disabled attribute for value
 */
function is_disabled($b) {
    return $b ? ' disabled="disabled"' : '';
}

/**
 * Generate a select from a table
 */
function get_html_select($name, $tbl_name, $col_value, $col_label, $selected='', $order_by='', $onchange='') {

    global $App;

    $qry = "SELECT $col_value, $col_label FROM $tbl_name";
    if ($order_by) {
        $qry .= ' ORDER BY ' . $order_by;
    }
    $rows = $App->db->get_results($qry);
    if (preg_match('/ AS /', $col_label)) {
        $col_label = preg_replace('/^.+ AS (.+)$/','$1', $col_label);
    }
    if (!$rows) {
        $App->add_server_error('get_html_select',
            'Nothing found in '.$tbl_name);
        $App->internalerror();
    }
    $str = '<select name="'.$name.'" id="'.$name.'"';

    if ($onchange) {
        $str .= ' onchange="'.$onchange.'"';
    }

    $str .= '>'."\n";
    foreach ($rows as $r) {
        $str .= '<option value="'.$r->$col_value.'"';
        $str .= $r->$col_value == $selected ? ' selected="selected">' :'>';
        $str .= _(column_display_label($r->$col_label)).'</option>'."\n";
    }
    return $str .= "\n".'</select>';
}

/**
 * Generate a select from a SQL ENUM field
 */
function get_html_enum_select($tbl_name, $col, $selected='', $in_array='', $any=false) {

    global $App;

    $options = $App->db->get_enum_options($tbl_name, $col);
    if (!$options) {
        $App->add_server_error('get_html_enum_select',
            'Nothing found for '.$col.' in '.$tbl_name);
        $App->internalerror();
    }

    if ($in_array) {
        $str = '<select name="'.$in_array.'['.$col.']" id="'.$in_array.'_'.$col.'">'."\n";
    } else {
        $str = '<select name="'.$col.'" id="'.$col.'">'."\n";
    }
    if ($any) {
        $str .= '<option value="">Any</option>';
    }
    if ($options[0] == 'NULL') {
        $str .= "\n".'<option value="NULL"';
        if ($selected == null && !$any) {
            $str .= ' selected="selected"';
        }
        $str .= ">Unknown</option>\n\n";
        $start = 1;
    } else {
        $start = 0;
    }
    for($i=$start;$i<count($options);$i++) {
        $o = $options[$i];
        $str .= '<option value="'.$o.'"';
        $str .= $o == $selected ? ' selected="selected">' :'>';
        $str .= _(column_display_label($o)).'</option>'."\n";
    }
    return $str .= "\n".'</select>';
}

/**
 * Convert an array like received in POST to an object
 */
function input2object($a) {

    $tmp = '';
    if (!count($a)) return false;
    foreach ($a as $k=>$v) {
        if (is_array($v)) {
            $tmp->$k = input2object($v);
        } else {
            if (is_numeric($k)) {
                $tmp[] = $v;
            } else {
                # Convert booleans
                if ($v == 'true') {
                    $tmp->$k = true;
                } else if ($v == 'false') {
                    $tmp->$k = false;
                } else {
                    $tmp->$k = trim($v);
                }
            }
        }
    }
    return $tmp;
}