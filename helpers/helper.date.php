<?php

/*-------------------------------------------------------------------
strtotime_uk($str)

Just like str to time, but for treats dates in UK style i.e. dd/mm/yy
*/
function strtotime_uk($str)
{
    $str = preg_replace(
           '/^\s*([\d]{1,2})[\/\. -]+([\d]{1,2})[\/\. -]*([\d]{0,4})/',
           '\\2/\\1/\\3', 
           $str);
    $str = trim($str,'/.-');
    return strtotime($str);
}

/*-------------------------------------------------------------------
date_format($d=false,$format='d/m/Y')

Works a bit like date(), except the date can be anything parseable by 
strtotime and if the passed in date is empty or doesn't look like a date
the date is returned unmodified (date() defaults to the current date).

The other big difference from date() is that arguments are reversed: the
date comes first then the format.

This is useful when you want to display a date but aren't sure if it
is valid or even present.

Has a default format of 'd/M/Y'
*/
function mydate_format($d=false, $format='d/m/Y', $alt='')
{
    # Check the date only contains letters, numbers and 
    # accepted delimiters. If not, just return the string.

    if(preg_match('/[^\w0-9:\/ -]/',$d)) return $d;
    if($d){ 
        if(!is_numeric($d)) {
            $ts = strtotime_uk($d);
        } else {
            $ts = $d;
        }
        if($ts === -1) return $d; # If invalid just return input
        return date($format,$ts);
    }
    return $alt;
}

/*-------------------------------------------------------------------
since($timestamp)

Returns a human readable date

This is borrowed from textpattern. It has been extended so a future 
date will come back as 'in [whatever]' and a past date will come back 
as '[whatever] ago'.

I'm also want to have weeks, months and years with fractions!
Shouldn't be too difficult.

seconds in...
a minute: 60
an hour (60*60): 3,600
a day (24*60*60): 86,400
a week (7*24*60*60): 604,800
a month, days in month being averaged (30.4*24*60*60): 2,626,560
a year (365*24*60*60): 31,536,000

This doesn't account for leap years and the like, it's just a rough idea.

*/

function since($stamp){
    $diff = (time() - $stamp);
    $future = $diff < 0;

    if($future) $diff*=-1;
    
    switch($diff){
        case $diff >= 2626560:
            $months = floor($diff / 2626560);
            $since = ($months <= 1) ? '1 month' : $months.' months';
            break;
       /* case $diff >= 604800:
            $weeks = round($diff / 604800);
            $since = ($weeks <= 1) ? '1 week' : $weeks.' weeks';
            break;*/
        case $diff >= 86400:
            $days = round($diff / 86400);
            $since = ($days <= 1) ? '1 day' : $days.' days';
            break;
        case $diff > 3600:
            $hours = round($diff / 3600);
            $since = ($hours <= 1) ? '1 hour' : $hours.' hours';
            break;
        case $diff <= 3600:
            $mins = round($diff / 60);
            $since = ($mins <= 1) 
            ?	($mins==1)
                ?	'1  minute'
                :	 'a few seconds' 
            :	$mins . ' minutes' ;

            break;
    }

    return $future ? "in $since" : "$since ago";
}
/*-------------------------------------------------------------------
since_span($date)

This extends since() so you can pass it any date that can be parsed 
with strtotime. It will wrap the date in a span that has the actual 
date as a title. The span will also have the class name 'date'.

*/

function since_span($date){
    
    $ts = strtotime_uk($date);    
    $str = since($ts);
    $nice_date = date('g:ia \o\n l, jS F Y',$ts);

    return '<span class="date" title="' . $nice_date . '">'.$str.'</span>';
}

/*-------------------------------------------------------------------
deadline($due,$completed,$due_prefix='Due',$completed_prefix='Done')

Pass it a due date and completed date (which might be empty). It'll
add the relevant prefix and wrap it in useful spans.

It also defaults to today if the date is today, as you don't need 
the granular detail.
*/
function deadline($due,$completed='',
                    $due_prefix='',$completed_prefix=''){
    
    #if(!$due) return '<em>No due date</em>';
    $due_ts = strtotime_uk($due);
    $completed_ts = strtotime_uk($completed); 
    $due_str = date_format($due);
    $completed_str = date_format($completed);

    if(date('d/m/Y',$due_ts) === date('d/m/Y',$completed_ts)){
        $due_str = $completed_str = 'today';
    }
    $str = '';
    $nice_date = $due_prefix.' '.date('g:ia l, jS F Y',$due_ts);

    if($completed){
        $str = $completed_prefix.' '.$completed_str;
        $nice_date = $completed_prefix.' '.date('g:ia l, jS F Y',$completed_ts);

    } else if($due_ts < time() && !$completed){
        $str = '<span class="late">'.$due_prefix . ' '.$due_str.'</span>';

    } else {
        $str = $due_prefix.' '.$due_str;
    }
    
    return '<span class="date" title="'.$nice_date.'">'.$str.'</span>';
}

/*-------------------------------------------------------------------*/

function mydate_diff($start,$end,$in='days'){

    $start_ts = strtotime_uk($start);
    $end_ts = strtotime_uk($end); 
    $diff = $end_ts-$start_ts;

    switch($in){
        case 'days':
            $days = ceil($diff / 86400);
            $diff = ($days <= 1) ? '1 day' : $days.' days';
            break;
    }
    return $diff;

}

/*-------------------------------------------------------------------*/
function mydate_range($start, $end, $format='d/m/Y') {
    if (!$end || $start == $end) {
        return mydate_format($start, $format);
    }
    return mydate_format($start, $format) . ' - ' . mydate_format($end, $format);  
}
/*-------------------------------------------------------------------*/

function get_days_in_month($month){
    
    $day = 31;
    $year = date('Y');
    while(!(checkdate($month,$day,$year)) && $day > 27){
        $day = $day-1;
    }

    return $day;
}
/*-------------------------------------------------------------------*/
function last_of_month($ts){
        return strtotime('-1 day 23:59:59',
                    strtotime('+1 month',
                            strtotime(date('1 M Y 00:00:00',$ts))
                    )
                );
    }
/**
 * Parse data filter
 * Will convert months to a date range, otherwise leaves everything else alone
 * This is only need because superdb doesn't handle it.
 */
function parse_date($s){
    $months = array(
        'january',
        'february',
        'march',
        'april',
        'may',
        'june',
        'july',
        'august',
        'september',
        'october',
        'november',
        'december',
        'jan',
        'feb',
        'mar',
        'apr',
        'may',
        'jun',
        'jul',
        'aug',
        'sep',
        'sept',
        'oct',
        'nov',
        'dec',
    );
    $tokens = explode(' ', trim($s));
    if (count($tokens) > 2) {
        return $s;
    }
    $mo = strtolower($tokens[0]);
    if (!in_array($mo, $months)) {
        return $s;
    }
    if ($tokens[1] && is_numeric($tokens[1])) {
        $mo .= ' ' . $tokens[1];
    }
    $start = strtotime("1 $mo");
    $end = last_of_month($start) .  ' ' . $mo;
    return date('d/m/Y', $start) . '-' . date('d/m/Y', $end);
}
