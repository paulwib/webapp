<?php

/*

This is the version that uses the old filtering system. The new one is slightly
more sophisticated in the input it accepts.

*/
/*----------------------------------------------------------------------------*/

class SuperDB extends ezSQL_mysql {

    function SuperDB($dbuser='', $dbpassword='',
                        $dbname='', $dbhost='localhost'){

        $this->quick_connect($dbuser, $dbpassword, $dbname, $dbhost);
        $this->all_selects = array();
        $this->options_cache = array();
    }

/*----------------------------------------------------------------------------*/

    function escape($str,$force_quote=false) {

        # Strip slashes only if needed
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }

        # Auto quote and trim strings
        if (!is_numeric($str) || $force_quote) {
           $str = "'".mysql_real_escape_string(trim($str))."'";
        }
        return $str;
    }

/*----------------------------------------------------------------------------*/
    function get_table_singular($tbl_name){
        return substr($tbl_name,-3) == 'ies' ?
                 preg_replace('/ies$/','y',$tbl_name) :
                    substr($tbl_name,0,-1);
    }

/*----------------------------------------------------------------------------*/

    function get_table_info($tbl_name){

        if(!isset($this->table_cache)){
                $this->table_cache = array();
        }

        if(isset($this->table_cache[$tbl_name])){
            $table = $this->table_cache[$tbl_name];
        } else {
            $table = $this->get_results("DESCRIBE $tbl_name;");
            if($table) {
                $this->table_cache[$tbl_name] = $table;
            }
        }

        return $table;
    }

/*----------------------------------------------------------------------------*/

    function get_columns($tbl_name,$add_tbl_prefix=false){

        $table = $this->get_table_info($tbl_name);
        if(!$table) return false;
        $cols = array();
        foreach($table as $col){
            $cols[] = $add_tbl_prefix ? $tbl_name.'.'.$col->Field : $col->Field;
        }
        return $cols;
    }

/*----------------------------------------------------------------------------*/

    function get_column_type($tbl_name,$col_name){

        $table = $this->get_table_info($tbl_name);
        if(!$table) return false;
        foreach($table as $col){
            if($col->Field == $col_name){
                return strtoupper(trim(preg_replace('/(\(.+\)|unsigned)/','',$col->Type)));
            }
        }
        return false;
    }
/*----------------------------------------------------------------------------

   This returns the basic type - might not be entirely accurate but useful
   enough for my purposes

*/
    function get_column_basic_type($tbl_name,$col_name){


        $type = $this->get_column_type($tbl_name,$col_name);

        switch($type){
            case 'TEXT':
            case 'MEDIUMTEXT':
            case 'LONGTEXT':
            case 'VARCHAR':
            case 'CHAR':
            case 'ENUM':
            case 'SET':
                return 'text';
            case 'TINYINT':
            case 'SMALLINT':
            case 'MEDIUMINT':
            case 'BIGINT':
            case 'INT':
            case 'DECIMAL':
            case 'FLOAT':
            case 'DOUBLE':
                return 'number';
            case 'YEAR':
            case 'MONTH':
            case 'DATETIME':
            case 'DATE':
            case 'TIMESTAMP':
                return 'date';
            case 'TIME':
                return 'time';
            default:
                return false;
        }
    }

/*----------------------------------------------------------------------------

    This returns the possible values of an ENUM or SET field
*/
    function get_enum_options($tbl_name,$col_name,$include_null=true){

        $col = $this->get_row("SHOW COLUMNS FROM $tbl_name LIKE '$col_name'");
        $str = preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$col->Type);
        $options = explode("','",$str);
        if($col->Null == 'YES' && $include_null) array_unshift($options,'NULL');

        return $options;
    }

/*----------------------------------------------------------------------------

    This returns the possible values of a linked table, using one field for the
    key and another for the value.
*/
    function get_linked_table_options($tbl_name,$key_col,$val_col){
        $options = array();
        $rows = $this->get_results("SELECT $key_col, $val_col FROM $tbl_name ORDER BY $val_col;");
        foreach($rows as $row){
            $options[$row->$key_col] = $row->$val_col;
        }

        return $options;
    }
/*----------------------------------------------------------------------------

    Converts any date convertible with strtotime into a SQL friendly one.
    Also works with numeric timestamps.
*/
    function strtosqldate($d){

       $d = trim($d);

       if($d === '') return 'NULL';

       if(is_numeric($d)){
            $ts = $d;
       } else {
           $ts = function_exists('strtotime_uk') ? strtotime_uk($d) :
                            strtotime($d);
       }
        if($ts && $ts != -1){
            return "'".date('Y-m-d H:i:s',$ts)."'";
        } else {
            return 'NULL';
        }
   }
/* As above but for time */
    function strtosqltime($d){
       $d = trim($d);

       if($d === '') return 'NULL';

       if(is_numeric($d)){
            $ts = $d;
       } else {
           $ts = strtotime($d);
       }
        if($ts && $ts != -1){
            return "'".date('H:i:s',$ts)."'";
        } else {
            return 'NULL';
        }
   }

/*----------------------------------------------------------------------------

    Given a data object this will perform an INSERT query with all values properly
    escaped. It uses reflection to make sure only column names that exist in the
    table will be added to the query. It follows these rules:

    * NOW() and NULL will not be escaped
    * auto_increment fields will always be ignored
    * DATETIME values can be anything that strtotime() can handle and will be
      converted to valid SQL dates.
    * Number fields that contain an empty string will be skipped (so as not to
      muck up table defaults).

*/
        function create($tbl_name,$data){

            $skip_escape = array('NOW()','NULL');
            $table = $this->get_table_info($tbl_name);
            $qry_values = array();

            foreach($table as $col){

                # Skip auto increments
                if($col->Extra == 'auto_increment') continue;

                $fld = $col->Field;

                if(isset($data[$fld])){

                    $type = $this->get_column_basic_type($tbl_name,$fld);

                    # Escape values
                    if(!in_array($data[$fld],$skip_escape)) {

                        # Text
                        if($type == 'text'){
                            $qry_values[$fld] = $this->escape($data[$fld],true);

                        # Date
                        } else if($type == 'date'){
                            $qry_values[$fld] = $this->strtosqldate($data[$fld]);

                        # Time
                        } else if($type == 'time'){
                            $qry_values[$fld] = $this->strtosqltime($data[$fld]);

                        # Number
                        } else if($type == 'number' && $data[$fld] !== ''){
                            $qry_values[$fld] = $data[$fld];
                        }

                    # Don't escape functions and NULL
                    } else {
                        $qry_values[$fld] = $data[$fld];
                    }
                }
            }
            # The query is formatted with whitespace for readability when debugging
            $qry = "INSERT INTO $tbl_name(".
                    implode(",\n\t\t\t",array_keys($qry_values)).
                    ")\n VALUES(".implode(",\n\t\t\t",$qry_values).");";

            $res = $this->get_results($qry);
            if($this->insert_id) return $this->insert_id;
            return false;
        }

/*----------------------------------------------------------------------------

    This works similar to create(), except it performs an UPDATE query.
    It takes a 3rd argument which should contain the WHERE clause e.g. 'id=4'
*/
        function update($tbl_name,$data,$where){

            $skip_escape = array('NOW()','NULL');
            $table = $this->get_table_info($tbl_name);
            $qry_updates = array();

            foreach($table as $col){

                if($col->Extra == 'auto_increment') continue;

                $fld = $col->Field;
                $skip = false;
                if(isset($data[$fld])){

                    $type = $this->get_column_basic_type($tbl_name,$fld);

                    # Escape value
                    if(!in_array($data[$fld],$skip_escape)) {

                        # Text
                        if($type == 'text'){
                            $value = $this->escape($data[$fld],true);

                        # Date
                        } else if($type == 'date'){
                            $value = $this->strtosqldate($data[$fld]);

                        # Time
                        } else if($type == 'time'){
                            $value = $this->strtosqltime($data[$fld]);

                        # Skip numbers with empty strings
                        } else if($type == 'number' && $data[$fld] === ''){
                            $skip = true;

                        } else {
                            $value = $data[$fld];
                        }

                    } else {
                        $value = $data[$fld];
                    }

                    if(!$skip) $qry_updates[] = "$fld=$value";
                }
            }

            $qry = "UPDATE $tbl_name SET ".
                   implode(",\n\t\t\t",$qry_updates).
                   "\n     WHERE $where;";

            $res = $this->get_results($qry);
            return $this->rows_affected;
        }
/*----------------------------------------------------------------------------

    Retrieve a single record by an exact value of a field. Options are:

    tbl_name
    column
    value
    rels (as below)
*/
    function retrieve($options){
        extract($options);
        $options['where'] = $column.'='.$this->escape($value);
        $options['ps'] = 1;
        $res = $this->search($options);
        return $res['data'] ? $res['data'] : false;

    }
/*----------------------------------------------------------------------------
Arguments passed as an array. Valid keys are:

tbl_name - the primary table
o - the column to order by
d - the direction to order by
ps - page size (the number of rows per page, 0 for everything)
p - the page number
q - a value to search for
rels - an array of relationships
f - an array of filters
column_map - maps the column names for filtering and searching from 'aliases'
to real table.column type values. This is useful for input from the web where
you can't or don't want to include column names in the query string.
where - an arbitary where clause

The rels is an array of arrays. Each rel can have the following properties:

linked_table - A string name of a linked table or a string of JOINs that
               can come after a SQL FROM
columns - an array of columns to return. These should be fully qualified with
          the table name to avoid collisions. Columns can be aliased. This
          can also be used for a subselect (in which case the linked table and
          rel isn't required).
type - one or many, which says whether the related table has many related
       rows or just one. Defaults to one. Also doesn't make any difference
       if you used a JOIN in linked_table

Filters is an array. It allows searching on multiple columns.
Only rows that match all fields will be returned. The key name is the
name of the column to search on and the value is the value to search for.

Because it's not always possible to use a dot (.) in an array key you should also
provide a column_map argument where the keys match the columns you're filtering on
and the values give the real column name e.g.

f = array('first_name','bob');
column_map = array('first_name'=>'customers.first_name');

If you try to filter without a column map and there is no dot in the filters
key name then it will have the main table name prepended. This might not always
be desirable so you should provide the column map.

Note that if filters are passed any 'q' search paramater will be ignored.

*/

    function search($options){

        $start_time = $this->microtime_float();

        extract($options);
        if(!isset($tbl_name)) return false;
        if(!isset($p) || !is_numeric($p)) $p = 1;
        if(!isset($ps) || !is_numeric($ps)) $ps = 30;
        $offset = ($p-1)*$ps;
        $limit = $ps;

        $fields = $this->get_columns($tbl_name,true);
        $from = array($tbl_name);

        if($rels){
            foreach($rels as $rel){

                # Columns from JOINed tables
                if(isset($rel['columns'])){
                    if(is_array($rel['columns'])){
                        $fields = array_merge($fields,$rel['columns']);
                    } else {
                        $fields[] = $rel['columns'];
                    }

                    if(isset($rel['linked_table'])){
                        $parent = $this->get_table_singular($tbl_name);
                        $child = $this->get_table_singular($rel['linked_table']);
                        $linked_table = $rel['linked_table'];

                        if(preg_match('/.+(JOIN).+/',$linked_table)){
                            $from[] = $linked_table;

                        } else if ($rel['type'] == 'many'){
                            if(!$group) $group = "$tbl_name._id";
                            $from[] = "LEFT JOIN $linked_table ON
                                  $tbl_name.id=$linked_table.{$parent}_id";
                        } else {
                            $from[] = "LEFT JOIN $linked_table ON
                                   $tbl_name.{$child}_id=$linked_table.id";
                        }
                    }
                } else {
                    $subobjects[] = $rel;
                }
            }
        }

        # Build query and get main rows (formatted for readability)
        $qry = $limit > 1 ? 'SELECT SQL_CALC_FOUND_ROWS ' : 'SELECT ';
        if($distinct) $qry .= ' DISTINCT ';
        $qry .= "\n  " . implode(",\n  ",$fields);
        $qry .= "\n  FROM " . implode(" \n  ",$from);

        # Add filter paramaters
        if($f){

            foreach($f as $k=>$v){

                if(!($field = $column_map[$k]['column'])){
                    $field = $k;
                }

                # If column name is unqualified the main table name is assumed
                if(!strpos($field,'.')){
                    $field = $tbl_name.'.'.$field;
                }
                $value = $v;

                if(preg_match('/^__agg__/',$field)){
                    $having = true;
                    list($foo,$type,$field) = explode('.',$field);
                    unset($foo);
                } else {
                    $having = false;
                    list($table,$column) = explode('.',$field);
                    $type = $this->get_column_basic_type($table,$column);
                    # Skip filters where column definitely doesn't exist
                    # Should maybe throw an error here rather than silently
                    # ignoring it.
                    if(!$type) continue;
                }

                $query = '';

                if($value == 'NULL') {
                    $query = "$field IS NULL";

                } else if($value == 'EMPTY'){
                    $query = "$field=''";

                } else if($value == 'NULL_OR_EMPTY'){
                    $query = "($field IS NULL OR $field='')";

                } else if($value == 'NOT_NULL') {
                    $query = "$field IS NOT NULL";

                } else if($value == 'NOT_EMPTY'){
                    $query = "$field !=''";

                } else if($value == 'NOT_NULL_OR_EMPTY'){
                    $query = "$field IS NOT NULL AND $field !=''";

                } else if($type == 'number'){

                    if(strpos($value,'-')){
                        list($min,$max) = explode('-',$value);
                        $min = trim($min); $max = trim($max);
                        if(is_numeric($min) && is_numeric($max) && $max>$min){
                            $query = $field.' BETWEEN '.$min.' AND '.$max;
                        }

                    } else if(strpos($value,'>') === 0){
                        $tmp_value = str_replace('>','',$value);
                        if(is_numeric($tmp_value)){
                            $query = $field.' > '.$tmp_value;
                        }

                    } else if(strpos($value,'<') === 0){
                        $tmp_value = str_replace('<','',$value);
                        if(is_numeric($tmp_value)){
                            $query = $field.' < '.$tmp_value;
                        }
                    } else if(strstr($value,',')){
                        $query = "$field IN (".$value.')';

                    } else if(is_numeric($value)){
                        $query = $field.' = '.$value;
                    }


                } else if($type == 'text'){
                    if(strpos($value,'!') === 0) {
                        $not = ' NOT';
                        $value = substr($value, 1);
                    } else {
                        $not = '';
                    }
                    if(strstr($value,'*')){
                        $tmp_value = str_replace('*','%',$value);
                        $query = $field.$not.' LIKE '.$this->escape("$tmp_value");

                    } else if(strstr($value,',')){
                        $tmp = explode(',',$value);
                        $tmp2 = array();
                        foreach($tmp as $v){
                            $tmp2[] = $this->escape(trim($v));
                        }
                        $query = $field.$not.' IN ('.implode(',',$tmp2).')';

                    } else {
                        $query = $field.$not.' LIKE '.$this->escape("%$value%");
                    }

                } else if($type == 'date'){
                    if(!trim($value)) continue; # Skip empty dates
                    if($value == 'never'){
                        $query = $field.' IS NULL';

                    } else if(strpos($value,'last') === 0){
                        $tmp_value = str_replace('last ','',$value);
                        $query = $field.' > '.$this->strtosqldate($tmp_value. ' ago');

                    } else if(strpos($value,'before') === 0 || strpos($value,'over') === 0){
                        $tmp_value = str_replace('before ','',$value);
                        $tmp_value = str_replace('over ','',$tmp_value);
                        $query = $field.' < '.$this->strtosqldate($tmp_value);

                    } else if(strpos($value,'after') === 0){
                        $tmp_value = str_replace('after ','',$value);
                        $query = $field.' > '.$this->strtosqldate($tmp_value);

                    } else if(strpos($value,'-')){
                        list($start,$end) = explode('-',$value);
                        $start = trim($start); $end = trim($end);
                        $end = $this->strtosqldate($end);
                            if(strpos($end,'00:00:00')) $end = str_replace('00:00:00','23:59:59',$end);
                            $query = $field.' BETWEEN '.
                                     $this->strtosqldate($start).
                                     ' AND '. $end;
                    } else {
                        $tmp_value = $this->strtosqldate($value);
                        $tmp_value = preg_replace('/[0-9][0-9]:[0-9][0-9]:[0-9][0-9]/','',$tmp_value);
                        $tmp_value = str_replace("'",'',$tmp_value);
                        $tmp_value = trim($tmp_value);
                        $query = $field.' LIKE '.$this->escape("$tmp_value%");
                    }
                }
                if($having){
                    $queries_having[] = $query;
                } else if($query) {
                    $queries_where[] = $query;
                }
            }
            if($queries_where){
                $qry .= "\n WHERE ".implode("\n\t AND ",$queries_where);
            }

        # Add search paramaters
        } else if($q = trim($q)){
            $q_type = is_numeric($q) ? 'number' : 'text';

            foreach($fields as $field){
                $alias = false;
                if(preg_match('/\(/',$field)){
                    # Skip searching on fields that are the result of functions
                    # or subselects
                    continue;
                } else if(strpos($field,' AS ')){
                    # If aliased extract the alias name and the field
                    $tmp = explode(' AS ',$field);
                    $field = trim($tmp[0]);
                    $alias = trim($tmp[1]);
                }
                list($table,$column) = explode('.',$field);

                if(!$table || !$column) continue;
                $type = $this->get_column_basic_type($table,$column);
                # Don't search numeric fields for text
                if($type == 'number' && $q_type == 'text') continue;
                #$col = $alias ? $alias : $table.'.'.$column;
                if($type == 'text'){
                    $queries_where[] = "$table.$column LIKE ".$this->escape("%$q%");
                } else if($type == 'number'){
                    $queries_where[] = "$table.$column LIKE ".$this->escape("$q");
                }
            }
            $qry .= "\n WHERE (".implode("\n\t OR ",$queries_where).')';
        }

        if($where){
            if(!$queries_where) {
                $qry .= "\n WHERE ";
            } else {
                $qry .= ' AND ';
            }
            $qry .= "\n\t$where";
        }

        if($group){
            $qry .= "\n GROUP BY $group";
        }

        if($queries_having){
            $qry .= "\n HAVING ".implode("\n\t AND ",$queries_having);
        }

        if($o){
            if($column_map[$o]) $o = $column_map[$o]['column'];
            if(!$d ||
                !in_array(strtoupper($d),array('ASC','DESC'))){
                $d = 'ASC';
            }
            $qry .= "\n ORDER BY $o $d";
        }

        if($limit){
            $qry .= "\n LIMIT $offset, $limit";
        }

        $qry .= ';';

        # This gets all the field names in the query
        # I could check they all existed before executing the query
        # if I felt like it
        #preg_match_all('/([a-z_]+\.[a-z_]+)/',$qry,$matches);
        #print_r($matches);
        $rs = $this->get_results($qry);
        if(!$rs[0]) return false;

        $found_rows = $this->get_var('SELECT FOUND_ROWS();');

        # If any relationships that require subobjects add them here
        if($subobjects){
            foreach($subobjects as $rel){
                $type = $rel['type'];
                if($type == 'one' || $type == 'many'){
                    # Function name generated dynamically
                    $rs = $this->{'has_'.$type}($rs,
                                                $tbl_name,
                                                $rel['linked_table'],
                                                $rel['parent_column'],
                                                $rel['child_column']);
                }
            }
        }

        # Make an array of nice column names
        foreach($rs[0] as $key=>$value){
            $columns[$key] = ucwords(str_replace('_',' ',$key));
        }

        # Get the number of found rows
        $ret['start_row'] = $offset+1;
        if(count($rs) < $limit && $offset == 0){
            $ret['end_row'] = count($rs);
        } else {
            $ret['end_row'] = min($offset+$limit,$found_rows);
        }
        $ret['found_rows'] = $found_rows;
        $ret['columns'] = $columns;
        $ret['data'] = $limit == 1 ? $rs[0] : $rs;
        $ret['page'] = $p;
        if($ps > 0) $ret['pages'] = ceil($found_rows/$ps);
        $ret['page_size'] = $ps;
        $ret['sql'] = $qry;
        $ret['execute_time'] = $this->microtime_float()-$start_time;
        $this->all_selects[] = $ret;

        return $ret;
    }
/*----------------------------------------------------------------------------

Pass a recordset and the singular name of the object each one has
This is all based on naming conventions. Normally this could be achieved
with a JOIN so try and figure that out before using this.
Each call generates one query.

The child table must have an id column that links to the parent table.
You can specify the name of the parent column with $join_on. I might
also make it possible to override the column used in the child table,
but for now it must be id.

*/
    function has_one($rs,$parent_table,$child_table,
                        $parent_column='',$child_column=''){

        if(!$child_column){
            $child_column = 'id';
        }

        if(!$parent_column){
            $ob_name = $this->get_table_singular($child_table);
            $parent_column = $ob_name.'_id';
        } else {
            $ob_name = $parent_column;
        }

        # Collect all the child ids
        $ids = array();
        foreach($rs as $r) {
            $id = $r->{$parent_column};
            if(!$id) continue;
            $ids[] = $id;
        }

        if(!$ids) return $rs;

        # Get the child rows
        $qry = 'SELECT * FROM '.$child_table.
               ' WHERE '.$child_column;

        if(count($ids) == 1){
           $qry .= ' = '.$ids[0].';';
        } else {
           $qry .= ' IN ('.
                implode(',',array_unique($ids)).');';
        }

        $rs2 = $this->get_results($qry);

        if(!$rs2) {
            return $rs;
        }

        # Put children in an array keyed by their id
        foreach($rs2 as $r){
            $tmp[$r->$child_column] = $r;
        }

        # Transfer them to their relevant parents
        $tmp2 = array();
        foreach($rs as $r){
            $r->{$ob_name} = $tmp[$r->{$parent_column}];
            $tmp2[] = $r;
        }

        return $tmp2;
    }

/*----------------------------------------------------------------------------

 For one-to-many relationships
*/
    function has_many($rs,$parent_table,$child_table,
                        $parent_column='',$child_column=''){

        $ob_name = $child_table;

        if(!$child_column){
            $child_column = $this->get_table_singular($parent_table).'_id';
        }

        if(!$parent_column){
            $parent_column = 'id';
        }

        # Collect all the child ids
        $ids = array();
        $rs1 = array();
        foreach($rs as $r) {
            $id = $r->{$parent_column};
            if(!$id) continue;
            $ids[] = $id;
            # Key parent recordset by the relevant column
            # (only works with numeric fields)
            $rs1[$r->{$parent_column}] = $r;
        }

        if(!$ids) return $rs;

        # Get the child rows
        $qry = 'SELECT * FROM '.$child_table.
               ' WHERE '.$child_column;

        if(count($ids) == 1){
           $qry .= ' = '.$ids[0].';';
        } else {
           $qry .= ' IN ('.
                implode(',',array_unique($ids)).');';
        }

        $rs2 = $this->get_results($qry);

        if(!$rs2) {
            return $rs;
        }

        # Transfer children to relevant parent's array
        foreach($rs2 as $r){
            $rs1[$r->{$child_column}]->{$ob_name}[] = $r;
        }

        # Put in a flat array
        $tmp = array();
        foreach($rs1 as $r){
            $tmp[] = $r;
        }

        return $tmp;
    }

/*--------------------------------------------------------------------------*/

    function options_get_all(){

        $tables = $this->get_col('SHOW TABLES');
        $this->options_cache = array();
        foreach($tables as $t){
            $cols = $this->get_table_info($t);
            foreach($cols as $c){
                if(preg_match('/^enum\(.+/',$c->Type)){
                    $tmp = '';
                    $tmp->table = $t;
                    $tmp->field = $c->Field;
                    $str = preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$c->Type);
                    $tmp->options = explode("','",$str);
                    $tmp->can_be_null = $c->Null == 'YES';
                    $this->options_cache[md5($t.$c->Field)] = $tmp;
                }
            }
        }
        return $this->options_cache;
    }

/*--------------------------------------------------------------------------*/

    function options_get($tbl_name,$col_name){

        $k = md5($tbl_name.$col_name);
        if($this->options_cache[$k]) return $this->options_cache[$k];

        $col = $this->get_row("SHOW COLUMNS FROM $tbl_name LIKE '$col_name'");
        if(!preg_match('/^enum\(.+/',$col->Type)) return false;

        $str = preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$col->Type);
        $options = explode("','",$str);
        $tmp = '';
        $tmp->table = $tbl_name;
        $tmp->field = $col_name;
        $tmp->options = $options;
        $tmp->can_be_null = $col->Null == 'YES';
        $this->options_cache[$k] = $tmp;
        return $tmp;
    }

/*--------------------------------------------------------------------------
  This will add or replace options. If the passed options isn't an array, it
  will be appended as a new option. If it is an array, the passed array of
  options will replace the current options.
  If you want to allow NULL values set the fourth argument to true.

  If a possible value is removed all rows with that value will be set to NULL.
*/

    function options_change($tbl_name,$col_name,$options,$is_null=false){
        $current_options = $this->options_get($tbl_name,$col_name);
        if(!$current_options) return false;
        if(!is_array($options)){
            array_push($current_options->options,$options);
            $options = $current_options->options;
        } else {
            $removed_options = array_diff($current_options->options,$options);
            if($removed_options){
                foreach($removed_options as $k=>$v){
                    $removed_options[$k] = $this->escape($v);
                }
                $this->get_results("UPDATE $tbl_name SET $col_name=NULL
                                    WHERE $col_name IN (".
                                   implode(',',$removed_options).');');
            }
        }
        sort($options);

        $qry = "ALTER TABLE $tbl_name CHANGE $col_name $col_name ENUM( ";
        foreach($options as $k=>$opt){
            $options[$k] = $this->escape($opt);
        }
        $qry .= implode(',',$options);
        $qry .= ')';
        if($is_null){
            $qry .= ' NULL DEFAULT NULL';
        }
        else {
            $qry .= ' NOT NULL';
        }
        $this->get_results($qry);
        return true;
    }

/*--------------------------------------------------------------------------*/

    function options_get_html_select($tbl_name,$col_name,$selected='',$in_array='',$class=''){

        $col = $this->options_get($tbl_name,$col_name);
        if(!$col) return false;
        $options = $col->options;

        if($in_array){
            $str = '<select name="'.$in_array.'['.$col_name.']" id="'.$in_array.'_'.$col_name.'"';
        } else {
            $str = '<select name="'.$col_name.'" id="'.$col_name.'"';
        }

        if($class){
            $str .= ' class="'.$class.'"';
        }

        $str .= ">\n";

        if($col->can_be_null){
            $str .= '\n<option value="NULL"';
            if($selected == null){
                $str .= ' selected="selected"';
            }
            $str .= ">Unknown</option>\n\n";
        }
        foreach($options as $o){
            $str .= '<option value="'.$o.'"';
            $str .= $o == $selected ? ' selected="selected">' :'>';
            $str .= $o.'</option>'."\n";
        }
        return $str .= "\n".'</select>';
    }


/*--------------------------------------------------------------------------*/

    function microtime_float(){
       list($usec, $sec) = explode(" ", microtime());
       return ((float)$usec + (float)$sec);
    }
}
?>
