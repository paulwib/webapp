<?php

/**
 * WebApp
 */

// Includes function for timing things
include('debug.php');

class WebApp {

    public $debug = true;
    public $db = false;
    public $content_types = array(
        'html'=>'text/html; charset=utf-8',
        'js'=>'application/x-javascript; charset=utf-8',
        'csv'=>'text/csv; charset=utf-8',
        'pdf'=>'application/pdf',
        'xml'=>'application/xml; charset=utf-8'
    );

    /**
     * Constructor
     *
     * @param string $app_name
     * @param string $base_url
     * @param string $view_dir Path to templates
     */
    public function __construct($app_name='', $base_url='', $view_dir=''){

        // Start output buffering
        ob_start();

        $this->app_name = $app_name ? $app_name : 'Mystery Application';
        $this->full_http_host = isset($_SERVER['HTTP_HOST']) ? 'http://'.$_SERVER['HTTP_HOST'] : false;
        $this->base_url = $base_url;
        $this->view_dir = $view_dir;
        $this->template_src = '';
        $this->template = new stdClass();
        $this->template->js_files = array();
        $this->template->css_files = array();
        $this->post_errors = '';
        $this->server_errors = '';
    }
    /**
     * Connect to database
     *
     * @param file $driver Must contain a function called dbconnect (urgh)
     */
    public function dbconnect($driver){
        if($this->db){
            $this->add_server_error('db', 'Tried to connect to database twice for one request');
            $this->internalerror();
        }
        if(is_file($driver)){
            require($driver);
            if(!function_exists('dbconnect')) {
              echo "function dbconnect must be defined in $driver\n";
              exit();
            }
            $this->db = dbconnect();
        }
        elseif($driver) {
          echo "Driver does not exist\n";
          exit();
        }
    }

    /**
     * Run the application
     *
     * @param array $urls Map of URL patterns to controllers
     */
    public function run($urls){

        $this->init_timers();
        $this->exec_start = microtime_float(); // timer
        $this->status = 200;

        // Remove base_url
        $rx = '/^'.preg_quote($this->base_url, '/').'/';
        $request_uri = preg_replace($rx, '', $_SERVER['REQUEST_URI']);

        // Remove query string before comparison
        $request_uri = preg_replace('/\?.*$/', '', $request_uri);

        $this->request_uri = $request_uri;

        // Get 'flavour'
        $fmatches = array();
        preg_match('/\..+/', $request_uri, $fmatches);
        $this->flavour = $fmatches ? substr($fmatches[0], 1) : 'html';

        // Redirect to base URL if this page was called directly
        if($request_uri == '/' . basename($_SERVER['SCRIPT_FILENAME'])) {
            $url = $this->base_url;
            $this->redirect($url, 301);
        }
        // Store the names of all controllers for creating top level navigation
        // Very useful while building, although would probably be replaced in a
        // production version.
        $controllers = array();

        // Prepare input
        $this->input = $this->prepare_input(array_merge($_GET, $_POST));
        $this->uploads = $this->prepare_uploads();

        // Try to match the URL
        foreach($urls as $url){

            $matches = array();
            // Controller name should be first token when splitting file by '.'
            $tmp = explode('.', preg_replace('/^_system\//', '', $url[1]));
            $cname = $tmp[0];

            if(preg_match($url[0], $request_uri, $matches)){

                // All matched controllers will be added to a body class
                // unless they have a 3rd element 'nobodyclass'
                if($url[2] != 'nobodyclass') $this->template->body_class .= $cname . ' ';

                // Execute the function for the given method passing the $matches
                $fnc = $cname . '_' . $_SERVER['REQUEST_METHOD'];

                // Check it exists first!
                if(!function_exists($fnc)){
                    $this->nomethod();
                }

                $fnc($matches, $input);
            }
            // Collect a list of controller names
            if($url[2] != 'nobodyclass') { $controllers[] = $cname; }
        }
        $this->template->controller_menu = $this->make_controller_menu(array_unique($controllers));
        $this->template->current_url = $this->full_http_host.$_SERVER['REQUEST_URI'];
        $this->exec_end = microtime_float(); // timer end

        // Output template
        if($this->template_src){
            $this->output();
        }

        $this->match_end = microtime_float(); // timer end
        $this->notfound();
    }

    /**
     * Output the response
     */
    public function output(){

        $this->output_start = microtime_float(); // timer

        if(!is_file($this->view_dir . $this->template_src)){
            $this->add_server_error('Template not found', $this->template_src);
            $this->internalerror();

        } else if($this->template) {

            // Copy template variables  and errors to local namespace
            // so available to templates without having to use
            // $this->template->whatever
            $this->template->app_name = $this->app_name;
            $this->template->post_errors = $this->post_errors;
            $this->template->server_errors = $this->server_errors;
            foreach($this->template as $k=>$v) $$k = $v;

            // Include the template, flush the output buffer and exit
            include($this->view_dir . $this->template_src);

            // PDFs can be generated with HTML2FPDF
            // There must be a corresponding HTML2FPDF class file.
            // The class name must always be 'CustomPDF'. It will then
            // generate the PDF from the output. All the template variables
            // are passed to the class
            if($this->flavour == 'pdf'){
                require($this->view_dir.'class.'.$this->template_src);
                $pdf = new CustomPDF($this->template);
                $pdf->AddPage();
                $pdf->WriteHTML(ob_get_contents());
                $tmp_fname = time().'.pdf';
                $pdf->output(TMP_DIR.$tmp_fname);
                // PDF callback function - useful if you want to do something
                // like merge the generated PDF with another PDF with pdftk
                // or something...
                // It must return the file name (it's free to change it)
                if(isset($this->pdf_callback)
                    && function_exists($this->pdf_callback)){
                        $fnc = $this->pdf_callback;
                        $fname = $fnc(TMP_DIR.$tmp_fname);
                        $this->output_fname = $fname;
                    }
                // Clear output buffer and fill with PDF data
                ob_end_clean();
                ob_start();
                echo file_get_contents(TMP_DIR.$tmp_fname);
                $this->force_download = true;
                unlink(TMP_DIR.$tmp_fname);
            }

            header('Content-Type:'.$this->content_types[$this->flavour]);

            // Force a download if needs be
            if($this->force_download){
                if(!isset($this->output_fname)){
                    $fname = str_replace('/', '_', $this->request_uri);
                    $fname = str_replace('_index', '', $fname);
                    $fname = trim($fname, '_');
                    if(!stristr($fname, '.'.$this->flavour)){
                        $fname .=  '.'.$this->flavour;
                    }
                } else {
                    $fname = $this->output_fname;
                }
                header("Content-Disposition: attachment; filename=$fname");
            }

            global $debug_messages;
            if($debug_messages){
                $jsstr = '';
                foreach($debug_messages as $msg){
                    $str = $msg;
                    // Escape single quotes
                    $str = str_replace("'", "\'", $str);
                    // Replace new lines
                    $str = str_replace("\n", '\n', $str);
                    $str = str_replace("\r", '\n', $str);
                    $jsstr .= "console.log('$str');\n";
                }
                $js = '<script type="text/javascript" defer="defer">';
                $js .= "\n<!--\n";
                $js .= "\nif(typeof(console) != 'undefined' && typeof(console.log) == 'function'){\n";
                $js .= $jsstr;
                $js .= "}\n//-->\n</script>\n";
                echo $js;
            }
            ob_end_flush();
            exit();
        }
    }

    /**
     * This prepares input data. It's passed the GET and POST arrays combined, so
     * they can all be accessed through $input. It does the following:
     *
     * - If the variable name ends in _x or _y a value will be added with the _x and _y
     *  removed. This is for when you use image submit buttons but might need to know
     *  the name of the input.
     * - Boolean looking strings are converted to true boolean values.
     * - All whitespace is trimmed from input values
     *
     */
    public function prepare_input($a){

        $tmp = '';
        if(!count($a)) return false;
        foreach($a as $k=>$v){
            if(is_array($v)) {
                $tmp[$k] = $this->prepare_input($v);
            } else {
                if(preg_match('/^.*(\.x|\.y|_x|_y)$/', $k)){
                    $tmp[$k] = $v;
                    $k = preg_replace('/(\.x|\.y|_x|_y)$/', '', $k);
                }
                $v = trim($v);

                // Convert booleans
                if($v == 'true'){
                    $tmp[$k] = true;
                } else if($v == 'false'){
                    $tmp[$k] = false;
                } else {
                    $tmp[$k] = $v;
                }
            }
        }
        return $tmp;
    }
    /**
     * The default PHP $_FILES array can be a bit confusing. If there is just one
     * file, or each file input is named differently it's OK, the $_FILES array key
     * is the upload input's name and it's value is an array of the five things
     * associated with an uploaded file: name, type, tmp_name, error, size.
     *
     * However, if the input name is an array you get a rather strange in $_FILES;
     * you get a similar array, but each value for name, type, tmp_name, error, size is
     * an array of all files. So the name element contains all the names, the type contains
     * all the types etc.
     *
     * This will sort out the $_FILES array so you get an array of objects with these
     * properties:
     *
     * - frm_name (the original name on the form, without brakets if an array)
     * - tmp_name
     * - name
     * - type
     * - error
     * - size
     *
     * It also skips empty files.
     */
    public function prepare_uploads(){

        if(!$_FILES) return false;

        $uploads = array();

        foreach($_FILES as $k=>$f){

            if(is_array($f['tmp_name'])){
                $tmp = array();
                foreach($f['tmp_name'] as $i=>$v){
                    if($f['error'][$i] == UPLOAD_ERR_NO_FILE) { continue; }
                        $tmp[$i]['frm_name'] = preg_replace('/\[|\]/', '', $k);
                    $tmp[$i]['tmp_name'] = $f['tmp_name'][$i];
                    $tmp[$i]['name'] = $f['name'][$i];
                    $tmp[$i]['type'] = $f['type'][$i];
                    $tmp[$i]['error'] = $f['error'][$i];
                    $tmp[$i]['size'] = $f['size'][$i];
                }
                $uploads = array_merge($uploads, $tmp);
            } else {
                if($f['error'] == UPLOAD_ERR_NO_FILE) { continue; }
                    $f['frm_name'] = $k;
                $uploads[] = $f;
            }
        }
        return $uploads;
    }

    /**
     * Set the HTTP status code
     */
    public function set_http_status($code, $url=false) {

        // Set HTTP status code
        if($code) {
            header("HTTP/1.1 $code", true, $code);
            $this->status = $code;
        }
        // Set Location: header if a URL is passed and the status code is a redirect
        if($url && $code >= 300 && $code < 400){
            header("Location: $url");
            exit();
        }
    }

    /**
     * Trigger a 400 Bad Request
     */
    public function badrequest(){

        $this->set_http_status(400);
        if(!is_file($this->view_dir . 'error_400.php')){
            exit('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                "http://www.w3.org/TR/html4/loose.dtd">
                <title>400 Bad Request</title>
                <h1>400 Bad Request</h1>
                <p>The request could not be understood by the server</p>
                <p>Additionally, an error document could not be found</p>');
        } else {
            $this->template->page_title = '400 Bad Request';
            $this->template_src = 'error_400.php';
        }
        $this->output();
    }

    /**
     * Trigger a 404 Not found
     */
    public function notfound(){
        $this->set_http_status(404);
        if(!is_file($this->view_dir . 'error_404.php')){
            exit('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                "http://www.w3.org/TR/html4/loose.dtd">
                <title>404 Page Not Found</title>
                <h1>404 Page not found</h1>
                <p><em>'.$this->request_uri.'</em> was not found on this server</p>
                <p>Additionally, an error document could not be found</p>');
        } else {
            $this->template->page_title = '404 Page Not Found';
            $this->template_src = 'error_404.php';
        }
        $this->output();
    }

    /**
     * Trigger a 405 Method not allowed
     */
    public function nomethod(){
        $this->set_http_status(405);
        if(!is_file($this->view_dir . 'error_405.php')){
            exit('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                "http://www.w3.org/TR/html4/loose.dtd">
                <title>405 Method Not Allowed</title>
                <h1>405 Method Not Allowed</h1>
                <p>The request method used is not supported for this URL and/or server</p>
                <p>Additionally, an error document could not be found</p>');
        } else {
            $this->template->page_title = '405 Method Not Allowed';
            $this->template_src = 'error_405.php';
        }
        $this->output();
    }

    /**
     * Trigger a 3x Redirect
     */
    public function redirect($url, $code=301){
        $this->set_http_status($code, $url);
    }

    /**
     * Trigger a 401 Authorization required
     */
    public function authrequired($auth_headers){
        $this->set_http_status(401);
        header($auth_headers);

        if(!is_file($this->view_dir . 'error_401.php')){
            exit('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                "http://www.w3.org/TR/html4/loose.dtd">
                <title>401 Authorization Required</title>
                <h1>401 Authorization Required</h1>
                <p>Authorization is required to view this page. Reload the page to
                enter your user name and password.</p>
                <p>Additionally, an error document could not be found</p>');
        } else {
            $this->template->page_title = '401 Authorization Required';
            $this->template_src = 'error_401.php';
        }
        $this->output();
    }

    /**
     * Trigger a 500 Internal error
     */
    public function internalerror(){
        $this->set_http_status(500);

        if($this->debug) {
            $this->template_src = 'error_debug_500.php';
            ob_start();
            debug_print_backtrace();
            $backtrace = preg_split("/\n//[0-9]/", ob_get_contents(), -1, PREG_SPLIT_NO_EMPTY);
            $this->template->backtrace = array_slice($backtrace, 1);
            ob_end_clean();
        } else {
            $this->template_src = 'error_500.php';
        }
        if(!is_file($this->view_dir . $this->template_src)){
            exit('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                "http://www.w3.org/TR/html4/loose.dtd">
                <title>500 Internal Server Error</title>
                <h1>500 Internal Server Error</h1>
                <p>The server encountered an internal error and cannot
                complete your request</p>
                <p>Additionally, an error document could not be found</p>');
        } else {
            $this->template->page_title = '500 Internal Server Error';
        }
        $this->output();
    }

    /**
     * Add a post error
     *
     * @param string $name
     * @param mixed $value
     */
    public function add_post_error($name, $value){
        $this->post_errors->$name = $value;
        $this->last_post_error = $value;
    }

    /**
     * Add a server error
     *
     * @param string $name
     * @param mixed $value
     */
    public function add_server_error($name, $value){
        $this->server_errors->$name = $value;
        $this->last_server_error = $value;
    }

    /**
     * Generate an HTML list of all controllers - good for a prototype menu
     */
    public function make_controller_menu($controllers){

        if(!$controllers || !$this->db) return false;
        sort($controllers);

        $str = "<ul>\n";
        foreach($controllers as $c){
            if(preg_match("/$c/", $this->request_uri)){
                $str.= "<li class=\"selected\">
                    <a href=\"$this->base_url/$c/\">".
                    table_display_title($c)."</a></li>";
            } else {
                $str.= "<li><a href=\"$this->base_url/$c/\">".
                    table_display_title($c)."</a></li>";
            }
        }

        return $str .= "</ul>";
    }

    /**
     * Initialise timers
     */
    public function init_timers(){
        $this->exec_start = 0;
        $this->exec_end = 0;
        $this->output_start = 0;
        $this->output_end = 0;
    }

    /**
     * Print some debug info
     */
    public function print_debug(){
        $this->output_end = microtime_float();
        $exec_time = $this->exec_end - $this->exec_start;
        $output_time = $this->output_end - $this->output_start;
        $total_time = round(($match_time+$exec_time+$output_time), 6);
        $txt_kb = round(strlen(ob_get_contents())/1024, 4);
        echo "\nExecute time: $exec_time
            \nOutput time:  $output_time
            \nTotal time:   $total_time
            \nText kb: $txt_kb\n";
    }
}
